#!/bin/bash
#MIT License
#Copyright (c) 2019 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir tre
cd tre
git init
#########################Parte 1###########################
printf "text A\n" > A
printf "text B\n" > B
git add .
git commit -m "root"
#########################Parte 2###########################
git branch uno #crea il branch uno ma resta su master
git checkout uno #passa al branch uno
printf "text C\n" > C
git add .
git commit -m "first"
#########################Parte 3###########################
git checkout master
mkdir D
printf "text E\n" > D/E
git mv A D/
git mv B D/
git add .
git commit -m "bis"
#########################Parte 4###########################
git merge uno -m "ooohhh"