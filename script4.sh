#!/bin/bash
#MIT License
#Copyright (c) 2019 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir quattro
cd quattro
git init
#########################Parte 1###########################
git checkout -b ZUT
mkdir R
printf "primo\n" > T
printf "linea\n" > R/S
git add .
git commit -m "feature RT"
#########################Parte 2###########################
git rm T
git rm R -r
mkdir Z
printf "primo\naggiunta\n" > T
printf "linea\n" > U
printf "linea\n" > Z/S
git add .
git commit -m "feature ZUT"
#########################Parte 3###########################
git checkout -b master
git rm -r *
git commit -m "end"
git checkout HEAD@{0}
printf "terzo\n" > 3
git hash-object -w 3