#!/bin/bash
#MIT License
#Copyright (c) 2018 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir uno
cd uno
git init
#########################Parte 1###########################
printf "text A\n" > A
printf "text B\n" > B
git add A B
git commit -m "uno"
#########################Parte 2###########################
git branch student #crea il branch student ma resta su master
git mv A D #rinomina A in D
git mv B C #rinomina B in C
git commit -m "due"
#########################Parte 3###########################
printf "text C\n" > A
printf "text C\n" > B
git add A B
mkdir _C
git mv C D _C
git mv _C C
git commit -m "tre"
git checkout student