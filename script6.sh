#!/bin/bash
#MIT License
#Copyright (c) 2019 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
cd due
#########################Parte 1###########################
git rm D -f
git checkout -b GOKU HEAD@{2}
git checkout student
git cherry-pick GOKU
#########################Parte 2###########################
git rm A B -f
git add .
git commit --no-edit
#########################Parte 3###########################
git cherry-pick master
git add .
git mv C~HEAD A
git mv D B
git commit --no-edit
#########################Parte 4###########################
git branch -D GOKU
git checkout master
git reset --hard student
git checkout student
git reset --hard HEAD^^