#!/bin/bash
#MIT License
#Copyright (c) 2019 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir cinque
cd cinque
git init
#########################Parte 1###########################
git checkout -b student
mkdir Z
printf "pluto\n" > X
printf "pippo\n" > Y
printf "pippo\n" > Z/Y
git add .
git commit -m "primo"
#########################Parte 2###########################
git rm Y
printf "pippo e minnie\n" > D
printf "pippo\n" > X
printf "pippo\n" > Z/Y
git add .
git commit -m "successivo"
#########################Parte 3###########################
git checkout -b master
mkdir G
git mv Z G/
git mv D G/
git mv X G/
git mv G Z
git add .
git commit -m "ultimo?"
git checkout student
#########################Parte 4###########################
mkdir G
git rm X
git mv D G/
git mv Z G/
git mv G Z