#!/bin/bash
#MIT License
#Copyright (c) 2019 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir 2019_01_28
cd 2019_01_28
git init
#########################Parte 1###########################
printf "pluto\n" > X
printf "pippo\n" > Y
git add .
git commit -m "primo"
git tag start
#########################Parte 2###########################
mkdir Z
printf "pippo\nminnie\n" > D
cp Y Z/
git mv X Z/
git add .
git commit -m "secondo"
git branch student
#########################Parte 3###########################
mkdir ZZ
git mv Z ZZ/
git mv Y ZZ/
git mv D ZZ/
git commit -m "terzo"
#########################Parte 4###########################
git checkout HEAD^
git rm Z/X
git rm Y
printf "paperino\n" > D
git add .