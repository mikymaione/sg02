#!/bin/bash
#MIT License
#Copyright (c) 2018 Michele Maione
#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#########################Parte 0###########################
mkdir due
cd due
git init
#########################Parte 1###########################
printf "text A\n" > A
printf "text B\n" > B
git add .
git commit -m "uno"
git branch student
#########################Parte 2###########################
mkdir C
git mv A C/D #rinomina A in D e mettilo in C
git mv B C/C #rinomina B in C e mettilo in C
printf "text C\n" > A
printf "text C\n" > B
git add .
git commit -m "tre"
#########################Parte 3###########################
git mv C X
git mv X/* .
git rm A
git rm B
rmdir X
git add .
git commit -m "due"
#########################Parte 4###########################
git reset --hard HEAD^
git checkout student
printf "text D\n" > D
git add .